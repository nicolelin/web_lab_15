package ictgradschool.web.lab15.ex05;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Servlet implementation class CGIParams
 */
public class ImageGalleryDisplay extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageGalleryDisplay() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        //response.getWriter().append("Served at: ").append(request.getContextPath());

        //displayBasic(request, response);

        displayHTML(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

    private void displayBasic(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // http://stackoverflow.com/questions/1928675/servletrequest-getparametermap-returns-mapstring-string-and-servletreques

        // https://docs.oracle.com/javaee/6/api/javax/servlet/ServletRequest.html

    }

    private void displayHTML(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // http://stackoverflow.com/questions/1928675/servletrequest-getparametermap-returns-mapstring-string-and-servletreques

        // https://docs.oracle.com/javaee/6/api/javax/servlet/ServletRequest.html

        // "/" is relative to the context root (your web-app name)
        // don't add your web-app name to the path

        //RequestDispatcher view = request.getRequestDispatcher("/response.html"); // works, but /response.jsp causes 500 "jsp support not configured"
        //view.forward(request, response);

        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n<head><meta charset=\"UTF-8\"><title>Image Gallery Viewer</title>");

        out.println("<!-- Latest compiled and minified CSS -->\n" +
                "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"\n" +
                "          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">");

        out.println("</head>\n<body>");

        out.println("<div class=\"container\">");

        out.println("<h1>Image Gallery Viewer</h1>");

        ServletContext servletContext = getServletContext();
        String fullPhotoPath = servletContext.getRealPath("/Photos");

        out.println("Displaying photos from: " + fullPhotoPath + "<br><br>");

        File[] files = new File(fullPhotoPath).listFiles();

        for (File file : files) {
            if (file.getName().contains("png")) {

                String imageThumb = file.getName();

                String imageFull = imageThumb.replaceAll("_thumbnail.png",".jpg");

                long fileSize = file.length();

                out.print("<a href=\"/Photos/" + imageFull + "\">");

                out.print("<img src=\"/Photos/"+ imageThumb +"\">");

                out.print("</a><br>");

                out.print("File size: " + fileSize + "<br><br>");

            }
        }

        out.println("</div>");

        out.println("</body></html>");

    }
}
